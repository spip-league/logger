<?php

namespace SpipLeague\Component\Logger;

use Monolog\Level;
use SpipLeague\Contracts\Filesystem\FilesystemInterface;

class Config
{
    public readonly int $permission;

    // @todo sapi, espace privé
    public function __construct(
        // Pour les fichiers
        public readonly string $siteDir,
        public readonly FilesystemInterface $filesystem,
        int $permission = 0644,
        public readonly string $log_path = 'tmp/log/%s.log',
        public readonly Level $max_level = Level::Notice,
        // Pour la rotation
        public readonly int $max_files = 4,
        public readonly int $max_size = 102_400,
        public readonly int $max_log = 100,
        // Pour les processeurs
        public readonly bool $fileline = false,
        public readonly bool $brut = false,
    ) {
        // Non executable
        $this->permission = $permission & ~0111;
    }

    public function logFile(string $logFile = 'spip'): string
    {
        if (!\preg_match(',^\w+$,', $logFile)) {
            $logFile = 'spip';
        }

        return sprintf(rtrim($this->siteDir, '/') . '/' . $this->log_path, $logFile);
    }
}
