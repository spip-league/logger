<?php

namespace SpipLeague\Component\Logger;

use Monolog\Formatter\LineFormatter as MonologLineFormatter;

class LineFormatter extends MonologLineFormatter
{
    public const SIMPLE_FORMAT = '%datetime% %extra.ip% (pid %extra.process_id%) %extra.backtrace%:%extra.espace%:%extra.level_name% %message%' . "\n";

    public const SIMPLE_DATE = 'Y-m-d H:i:s';
}
