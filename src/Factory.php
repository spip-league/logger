<?php

namespace SpipLeague\Component\Logger;

use Monolog\Formatter\FormatterInterface;
use Monolog\Handler\HandlerInterface;
use Monolog\Logger;
use Monolog\Processor\ProcessIdProcessor;
use Monolog\Processor\PsrLogMessageProcessor;
use Monolog\Processor\WebProcessor;
use SpipLeague\Component\Logger\Processor\BacktraceProcessor;
use SpipLeague\Component\Logger\Processor\BrutProcessor;
use SpipLeague\Component\Logger\Processor\EspaceProcessor;
use SpipLeague\Component\Logger\Processor\LevelNameProcessor;

class Factory
{
    public function __construct(
        private readonly Config $config,
        private readonly ?FormatterInterface $formatter = null,
        private readonly bool $test_espace_prive = false,
    ) {}

    /**
     * Créer un logger à partir du nom du fichier.
     */
    public function createFromFilename(string $filename): Logger
    {
        return new Logger($filename, $this->createHandlerStack($filename), $this->createProcessorStack());
    }

    /**
     * @return array<callable(\Monolog\LogRecord): \Monolog\LogRecord>
     */
    protected function createProcessorStack()
    {
        return [
            new BacktraceProcessor($this->config->fileline),
            new BrutProcessor($this->config->brut),
            new LevelNameProcessor(),
            new ProcessIdProcessor(),
            new WebProcessor(),
            new EspaceProcessor((string) \PHP_SAPI, $this->test_espace_prive),
            new PsrLogMessageProcessor(),
        ];
    }

    protected function createHandler(string $name): HandlerInterface
    {
        $handler = new RotatingFileHandler($this->config->logFile($name), $this->config);
        if ($this->formatter !== null) {
            $handler->setFormatter($this->formatter);
        }

        return $handler;
    }

    /**
     * @return list<HandlerInterface>
     */
    protected function createHandlerStack(string $name): array
    {
        $handlers = [$this->createHandler($name)];

        if ($name != 'spip') {
            $handlers[] = $this->createHandler('spip');
        }

        return $handlers;
    }
}
