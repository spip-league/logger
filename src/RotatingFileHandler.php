<?php

namespace SpipLeague\Component\Logger;

use Monolog\Handler\HandlerInterface;
use Monolog\Handler\StreamHandler;
use Monolog\LogRecord;
use Monolog\Utils;
use SpipLeague\Contracts\Filesystem\FilesystemInterface;

class RotatingFileHandler extends StreamHandler implements HandlerInterface
{
    protected string $filename;

    protected FilesystemInterface $filesystem;

    /**
     * @var int Taille en octects à atteindre pour effetuer une rotation (0=pas de rotation)
     */
    protected int $maxSize;

    /**
     * @var int Nombre maximum de messages pouvant être reçus par requête
     */
    protected int $maxLog;

    protected ?bool $mustRotate = false;

    /**
     * @var int Nombre maximum de fichiers à conserver
     */
    private readonly int $maxFiles;

    /**
     * @var int Nombre de messages reçus par requête
     */
    private int $logCount;

    public function __construct(
        string $filename,
        Config $config,
        bool $bubble = true,
        bool $useLocking = false,
    ) {
        $this->filename = Utils::canonicalizePath($filename);
        $this->filesystem = $config->filesystem;
        $this->maxFiles = $config->max_files;
        $this->maxSize = $config->max_size;
        $this->maxLog = $config->max_log;
        $this->logCount = 0;

        parent::__construct($filename, $config->max_level, $bubble, $config->permission, $useLocking);
    }

    /**
     * @codeCoverageIgnore
     */
    public function close(): void
    {
        parent::close();

        if ($this->mustRotate === true) {
            $this->logCount = 0;
            $this->rotate();
        }
    }

    /**
     * @codeCoverageIgnore
     */
    public function reset(): void
    {
        parent::reset();

        if ($this->mustRotate === true) {
            $this->logCount = 0;
            $this->rotate();
        }
    }

    protected function write(LogRecord $record): void
    {
        // on the first record written, if the log is new, we should rotate (once per day)
        // if (null === $this->mustRotate) {
        //     $this->mustRotate = null === $this->url || !file_exists($this->url);
        // }

        if ($this->maxSize > 0 && $this->filesystem->size($this->filename) > $this->maxSize) {
            $this->mustRotate = true;
            $this->close();
        }

        if ($this->maxLog == 0 || ++$this->logCount <= $this->maxLog) {
            parent::write($record);
        }
    }

    protected function rotate(): void
    {
        // skip GC of old logs if files are unlimited
        if ($this->maxFiles === 0) {
            return;
        }

        $rotate = $this->maxFiles;
        if ($rotate-- > 0) {
            $lock = $this->filesystem->getLockFactory()
                ->createLock($this->filename);
            if ($lock->acquire()) {
                $this->filesystem->remove($this->filename . '.' . $rotate);
                while ($rotate--) {
                    if ($this->filesystem->exists($this->filename . ($rotate ? '.' . $rotate : ''))) {
                        $this->filesystem->rename(
                            $this->filename . ($rotate ? '.' . $rotate : ''),
                            $this->filename . '.' . ($rotate + 1),
                        );
                    }
                }

                $lock->release();
            }
        }

        $this->mustRotate = false;
    }
}
