<?php

namespace SpipLeague\Component\Logger\Processor;

use Monolog\LogRecord;
use Monolog\Processor\ProcessorInterface;

class EspaceProcessor implements ProcessorInterface
{
    public function __construct(
        private readonly string $sapi,
        private readonly bool $test_espace_prive,
    ) {}

    public function __invoke(LogRecord $record): LogRecord
    {
        $record->extra['espace'] = $this->sapi == 'cli' ? 'Cli' : ($this->test_espace_prive ? 'Pri' : 'Pub');

        return $record;
    }
}
