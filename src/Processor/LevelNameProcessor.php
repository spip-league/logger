<?php

namespace SpipLeague\Component\Logger\Processor;

use Monolog\LogRecord;
use Monolog\Processor\ProcessorInterface;

class LevelNameProcessor implements ProcessorInterface
{
    public const SPIP_LEVEL_NAME = [
        'EMERGENCY' => 'HS:',
        'ALERT' => 'ALERTE:',
        'CRITICAL' => 'CRITIQUE:',
        'ERROR' => 'ERREUR:',
        'WARNING' => 'WARNING:',
        'NOTICE' => '!INFO:',
        'INFO' => 'info:',
        'DEBUG' => 'debug:',
    ];

    public function __invoke(LogRecord $record): LogRecord
    {
        $record->extra['level_name'] = self::SPIP_LEVEL_NAME[$record->level->getName()];

        return $record;
    }
}
