<?php

namespace SpipLeague\Component\Logger\Processor;

use Monolog\Level;
use Monolog\LogRecord;
use Monolog\Processor\IntrospectionProcessor;

class BacktraceProcessor extends IntrospectionProcessor
{
    public function __construct(
        private readonly bool $fileline = false,
        int|string|Level $level = Level::Debug,
        array $skipClassesPartials = [],
        int $skipStackFramesCount = 0,
    ) {
        parent::__construct($level, $skipClassesPartials, $skipStackFramesCount);
    }

    public function __invoke(LogRecord $record): LogRecord
    {
        if ($this->fileline) {
            $record = parent::__invoke($record);

            /** @var array{file?:string,line?:string,class?:string,callType:string,function:string} */
            $extra = $record->extra;
            $file = $extra['file'] ?? '';
            // @todo Filesystem+$siteDir plutôt que absolute('')
            // if (str_starts_with($file, absolute(''))) {
            // $file = substr($file, strlen(absolute('')) + 1);
            // }
            $line = $extra['line'] ?? '';
            $class = $extra['class'] ?? '';
            $function = $class ?
                $class . $extra['callType'] . $extra['function'] :
                $extra['function'];
            $function .= $function ? '()' : '';
            $record->extra['backtrace'] = "{$file}:L{$line}:{$function}: ";
        }

        return $record;
    }
}
