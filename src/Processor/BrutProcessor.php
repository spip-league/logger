<?php

namespace SpipLeague\Component\Logger\Processor;

use Monolog\LogRecord;
use Monolog\Processor\ProcessorInterface;

class BrutProcessor implements ProcessorInterface
{
    public function __construct(
        private readonly bool $brut = false,
    ) {}

    public function __invoke(LogRecord $record): LogRecord
    {
        if (!$this->brut) {
            $record = $record->with(message: str_replace('<', '&lt;', $record->message));
        }

        return $record;
    }
}
