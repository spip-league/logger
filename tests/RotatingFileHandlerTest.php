<?php

namespace SpipLeague\Test\Component\Logger;

use Monolog\Logger;
use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\Attributes\DataProvider;
use SpipLeague\Component\Logger\Config;
use SpipLeague\Component\Logger\RotatingFileHandler;
use SpipLeague\Sdk\Mock\MockSizeFilesystem;
use Symfony\Component\Filesystem\Filesystem;

#[CoversClass(Config::class)]
#[CoversClass(RotatingFileHandler::class)]
class RotatingFileHandlerTest extends TestCase
{
    protected function setUp(): void
    {
        (new Filesystem())->remove('tmp');
    }

    protected function tearDown(): void
    {
        (new Filesystem())->remove('tmp');
    }

    /**
     * @return array<string,array{expectedText: string, expectedCount: int, maxFiles: int, maxLog: int, messages: array<int,string>}>
     */
    public static function dataRotation(): array
    {
        return [
            'first' => [
                'expectedText' => 'test.WARNING: first',
                'expectedCount' => 1,
                'maxFiles' => 2,
                'maxSize' => 70,
                'maxLog' => 0,
                'messages' => ['first'],
            ],
            'big-a-lot' => [
                'expectedText' => 'test.WARNING: big',
                'expectedCount' => 10,
                'maxFiles' => 0,
                'maxSize' => 0,
                'maxLog' => 10,
                'messages' => array_fill(0, 11, 'big'),
            ],
            'big-2' => [
                'expectedText' => 'test.WARNING: big2',
                'expectedCount' => 1,
                'maxFiles' => 2,
                'maxSize' => 63,
                'maxLog' => 100,
                'messages' => array_fill(1, 5, 'big2'),
            ],
            'never-rotate' => [
                'expectedText' => 'test.WARNING: never-rotate',
                'expectedCount' => 500,
                'maxFiles' => 0,
                'maxSize' => 1,
                'maxLog' => 0,
                'messages' => array_fill(1, 500, 'never-rotate'),
            ],
        ];
    }

    /**
     * @param array<string> $messages
     */
    #[DataProvider('dataRotation')]
    public function testRotation(
        string $expectedText,
        int $expectedCount,
        int $maxFiles,
        int $maxSize,
        int $maxLog,
        array $messages,
    ): void {
        // Given
        $fs = new MockSizeFilesystem(100, \true, '(mock)', \true, \true, \true, \true, \false, 0, \null);
        if ($maxFiles > 0) {
            $fs->setSizes(...[0, 64, 64, 64, 64]);
        }
        $handler = new RotatingFileHandler('tmp/log/test.log', $this->getConfig([
            'filesystem' => $fs,
            'max_files' => $maxFiles,
            'max_size' => $maxSize,
            'max_log' => $maxLog,
        ]));
        $logger = new Logger('test', [$handler]);

        //  When
        foreach ($messages as $i => $message) {
            $logger->warning($message . '-' . $i);
        }

        // Then
        $lines = file('tmp/log/test.log');
        $this->assertIsArray($lines);
        $this->assertStringContainsString($expectedText, $lines[0]);
        $this->assertEquals($expectedCount, count($lines));
    }
}
