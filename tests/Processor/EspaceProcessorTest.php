<?php

namespace SpipLeague\Test\Component\Logger\Processor;

use Monolog\Level;
use Monolog\LogRecord;
use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\Attributes\DataProvider;
use SpipLeague\Component\Logger\Processor\EspaceProcessor;
use SpipLeague\Test\Component\Logger\TestCase;

#[CoversClass(EspaceProcessor::class)]
class EspaceProcessorTest extends TestCase
{
    /**
     * @return array<string,array{expected: string, sapi: string, test_espace_prive: bool}>
     */
    public static function dataInvoke()
    {
        return [
            'cli' => [
                'expected' => 'Cli',
                'sapi' => 'cli',
                'test_espace_prive' => false,
            ],
            'site-public' => [
                'expected' => 'Pub',
                'sapi' => 'whatever',
                'test_espace_prive' => false,
            ],
            'site-prive' => [
                'expected' => 'Pri',
                'sapi' => 'whatever',
                'test_espace_prive' => true,
            ],
        ];
    }

    #[DataProvider('dataInvoke')]
    public function testInvoke(string $expected, string $sapi, bool $test_espace_prive): void
    {
        // Given
        $processor = new EspaceProcessor($sapi, $test_espace_prive);
        $now = new \DateTimeImmutable();
        $record = new LogRecord($now, 'test', Level::Debug, '<test>');

        // When
        $actual = $processor($record);

        // Then
        $this->assertEquals($expected, $actual->extra['espace']);
    }
}
