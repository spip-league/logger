<?php

namespace SpipLeague\Test\Component\Logger\Processor;

use Monolog\Level;
use Monolog\LogRecord;
use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\Attributes\DataProvider;
use SpipLeague\Component\Logger\Processor\BacktraceProcessor;
use SpipLeague\Test\Component\Logger\TestCase;

#[CoversClass(BacktraceProcessor::class)]
class BacktraceProcessorTest extends TestCase
{
    /**
     * @return array<string,array{expected: string|null, fileline: bool}>
     */
    public static function dataInvoke()
    {
        return [
            'default' => [
                'expected' => \null,
                'fileline' => false,
            ],
            'forced' => [
                'expected' => ':L:SpipLeague\Test\Component\Logger\Processor\BacktraceProcessorTest->testInvoke(): ',
                'fileline' => true,
            ],
        ];
    }

    #[DataProvider('dataInvoke')]
    public function testInvoke(mixed $expected, bool $fileline): void
    {
        // Given
        $processor = new BacktraceProcessor($fileline);
        $now = new \DateTimeImmutable();
        $record = new LogRecord($now, 'test', Level::Debug, '<test>');

        // When
        $actual = $processor($record);

        // Then
        $this->assertEquals($expected, $actual->extra['backtrace']);
    }
}
