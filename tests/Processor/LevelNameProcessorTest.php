<?php

namespace SpipLeague\Test\Component\Logger\Processor;

use Monolog\Level;
use Monolog\LogRecord;
use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\Attributes\DataProvider;
use SpipLeague\Component\Logger\Processor\LevelNameProcessor;
use SpipLeague\Test\Component\Logger\TestCase;

#[CoversClass(LevelNameProcessor::class)]
class LevelNameProcessorTest extends TestCase
{
    /**
     * @return array<string,array{expected: string, level: Level}>
     */
    public static function dataInvoke()
    {
        return [
            'default' => [
                'expected' => 'debug:',
                'level' => Level::Debug,
            ],
            'forced' => [
                'expected' => 'HS:',
                'level' => Level::Emergency,
            ],
        ];
    }

    #[DataProvider('dataInvoke')]
    public function testInvoke(string $expected, Level $level): void
    {
        // Given
        $processor = new LevelNameProcessor();
        $now = new \DateTimeImmutable();
        $record = new LogRecord($now, 'test', $level, '<test>');

        // When
        $actual = $processor($record);

        // Then
        $this->assertEquals($expected, $actual->extra['level_name']);
    }
}
