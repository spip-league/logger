<?php

namespace SpipLeague\Test\Component\Logger\Processor;

use Monolog\Level;
use Monolog\LogRecord;
use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\Attributes\DataProvider;
use SpipLeague\Component\Logger\Processor\BrutProcessor;
use SpipLeague\Test\Component\Logger\TestCase;

#[CoversClass(BrutProcessor::class)]
class BrutProcessorTest extends TestCase
{
    /**
     * @return array<string,array{expected: string, brut: bool}>
     */
    public static function dataInvoke(): array
    {
        return [
            'default' => [
                'expected' => '&lt;test>',
                'brut' => false,
            ],
            'forced' => [
                'expected' => '<test>',
                'brut' => true,
            ],
        ];
    }

    #[DataProvider('dataInvoke')]
    public function testInvoke(string $expected, bool $brut): void
    {
        // Given
        $processor = new BrutProcessor($brut);
        $now = new \DateTimeImmutable();
        $record = new LogRecord($now, 'test', Level::Debug, '<test>');

        // When
        $actual = $processor($record);

        // Then
        $this->assertEquals($expected, $actual->message);
    }
}
