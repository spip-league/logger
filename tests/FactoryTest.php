<?php

namespace SpipLeague\Test\Component\Logger;

use Monolog\Formatter\NormalizerFormatter;
use Monolog\Handler\StreamHandler;
use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\Attributes\DataProvider;
use SpipLeague\Component\Logger\Config;
use SpipLeague\Component\Logger\Factory;
use SpipLeague\Component\Logger\Processor\BacktraceProcessor;
use SpipLeague\Component\Logger\Processor\BrutProcessor;
use SpipLeague\Component\Logger\Processor\EspaceProcessor;
use SpipLeague\Component\Logger\RotatingFileHandler;

#[CoversClass(Config::class)]
#[CoversClass(Factory::class)]
#[CoversClass(BacktraceProcessor::class)]
#[CoversClass(BrutProcessor::class)]
#[CoversClass(EspaceProcessor::class)]
#[CoversClass(RotatingFileHandler::class)]
class FactoryTest extends TestCase
{
    private Factory $factory;

    private Config $config;

    protected function setUp(): void
    {
        $this->config = $this->getConfig();
        $this->factory = new Factory($this->config);
    }

    /**
     * @return array<string,array<string,string>>
     */
    public static function dataCreateFromFilename(): array
    {
        return [
            'wrong-filename' => [
                'expected' => 'spip',
                'filename' => '/etc/passwd',
            ],
            'correct-filename' => [
                'expected' => 'test',
                'filename' => 'test',
            ],
        ];
    }

    #[DataProvider('dataCreateFromFilename')]
    public function testCreateFromFilename(string $expected, string $filename): void
    {
        // Given
        // $this->factory

        // When
        $actual = $this->factory->createFromFilename($filename);

        /** @var StreamHandler[] */
        $actual = $actual->getHandlers();

        // Then
        $this->assertInstanceOf(StreamHandler::class, $actual[0]);
        $this->assertStringContainsString(sprintf('tmp/log/%s.log', $expected), $actual[0]->getUrl() ?: '');
    }

    public function testLoggerWithFormatter(): void
    {
        // Given
        $factory = new Factory($this->config, new NormalizerFormatter());

        // When
        $logger = $factory->createFromFilename('test');

        /** @var StreamHandler[] */
        $actual = $logger->getHandlers();
        $actual = $actual[0]->getFormatter();

        // Then
        $this->assertInstanceOf(NormalizerFormatter::class, $actual);
    }
}
