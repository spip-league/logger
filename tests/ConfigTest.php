<?php

namespace SpipLeague\Test\Component\Logger;

use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\Attributes\DataProvider;
use SpipLeague\Component\Logger\Config;

#[CoversClass(Config::class)]
class ConfigTest extends TestCase
{
    /**
     * @return array<string,array<string,string>>
     */
    public static function dataLogFile(): array
    {
        return [
            'wrong-name' => [
                'expected' => dirname(__DIR__) . '/tmp/log/spip.log',
                'name' => ';',
            ],
            'other-valid-name' => [
                'expected' => dirname(__DIR__) . '/tmp/log/test.log',
                'name' => 'test',
            ],
        ];
    }

    #[DataProvider('dataLogFile')]
    public function testLogFile(string $expected, string $name): void
    {
        // Given
        $config = $this->getConfig();

        // When
        $actual = $config->logFile($name);

        // Then
        $this->assertEquals($expected, $actual);
    }
}
