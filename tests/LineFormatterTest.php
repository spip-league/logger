<?php

namespace SpipLeague\Test\Logger;

use Monolog\Level;
use Monolog\LogRecord;
use PHPUnit\Framework\Attributes\CoversClass;
use SpipLeague\Component\Logger\LineFormatter;
use SpipLeague\Test\Component\Logger\TestCase;

#[CoversClass(LineFormatter::class)]
class LineFormatterTest extends TestCase
{
    private LineFormatter $lineFormatter;

    protected function setUp(): void
    {
        $this->lineFormatter = new LineFormatter();
    }

    public function testFormat(): void
    {
        // Given
        $now = new \DateTimeImmutable();
        $record = new LogRecord($now, 'test', Level::Notice, 'test', [], [
            'ip' => '1.2.3.4',
            'process_id' => '1234',
            'espace' => 'Pub',
            'level_name' => '!INFO:',
        ]);

        // When
        $actual = $this->lineFormatter->format($record);

        // Then
        $expected = $now->format('Y-m-d H:i:s') . ' 1.2.3.4 (pid 1234) :Pub:!INFO: test' . "\n";
        $this->assertEquals($expected, $actual);
    }
}
