<?php

namespace SpipLeague\Test\Component\Logger;

use PHPUnit\Framework\TestCase as FrameworkTestCase;
use SpipLeague\Component\Logger\Config;
use SpipLeague\Sdk\Stub\StubFilesystem;
use SpipLeague\Contracts\Filesystem\FilesystemInterface;

/**
 * @internal
 *
 * @coversNothing
 */
class TestCase extends FrameworkTestCase
{
    protected function getFilesystemStub(): StubFilesystem
    {
        return new StubFilesystem(0, \true, '(stub)', \true, \true, \true, \true, \false, 0, \null);
    }

    /**
     * @param array{siteDir?:string,filesystem?:FilesystemInterface,max_files?:int,max_size?:int,max_log?:int} $loggerConfig
     */
    protected function getConfig(array $loggerConfig = []): Config
    {
        $loggerConfig = array_merge([
            'siteDir' => dirname(__DIR__),
            'filesystem' => $this->getFilesystemStub(),
        ], $loggerConfig);

        return new Config(...$loggerConfig);
    }
}
