# Changelog

## Unreleased

### Added

- CI

### Fixed

- Certains typages & phpdoc

## 2.0.2 - 2024-12-25

### Fixed

- Correction de paramètre en readonly

## 2.0.1 - 2024-12-21

### Added

- use `spip-league/filesystem-contracts` & `spip-league/sdk`
- Compat PHP 8.4

## 2.0.0 - 2024-07-18

- namespace PHP de `Spip` à `SpipLeague`
- PHP 8.2 mini

## 1.0.2 - 2024-02-01

### Fixed

- #2 Ajout d'un lock pour éviter plusieurs rotate() en même temps

## 1.0.1 - 2024-01-10

### Fixed

- #1 Formatter les variables de messages psr3, tel que `{name}` dans le logger
