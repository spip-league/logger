# spip-league/logger

## Installation

```bash
composer require spip-league/logger
```

## Usage

```php
use SpipLeague\Component\Logger\Config;
use SpipLeague\Component\Logger\Factory;

$config = new Config('.', $filesystem, ...[
    'log_path' => 'var/log/%.log',
    'max_files' => 10,
    'permission' => 0777
]);
$factory = new Factory($config);

$logger = $factory->createFromFile('my_logs');

$logger->error('error message');
```

```txt
// ./var/log/my_logs.log
[2023-06-13T01:36:37.676792+02:00] my_logs.ERROR: error message [] {"level_name":"ERREUR:","process_id":27613,"espace":"Cli"}
```
